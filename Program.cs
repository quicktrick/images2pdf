﻿// Important! Install Nuget package PdfSharp https://www.nuget.org/packages/PdfSharp/1.50.4000-beta3b

using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.IO;

namespace images2pdf
{
    class Program
    {
        static int startPage = 1;
        static int endPage = 500;
        static string sourceDir = @"d:\Languages\Dictionaries\DATA\French\Grand Larousse de la langue française\Pages\Vol7\";
        static string sourceFileTemplate = "page{0:0000}.jpg";
        static string destinationDir = @"d:\Languages\Dictionaries\DATA\French\Grand Larousse de la langue française\Pages\";
        static string destinationFile = "Grand Larousse de la langue française.Tome 7.pdf";

        static void Main(string[] args)
        {
            PdfDocument doc = new PdfDocument();
            for (int i = startPage; i <= endPage; i++)
            {
                PdfPage page = doc.AddPage();
                XImage xIimage = XImage.FromFile(GetSourcePath(i));
                page.Width = xIimage.PointWidth;
                page.Height = xIimage.PointHeight;
                XGraphics xgr = XGraphics.FromPdfPage(page);
                xgr.DrawImage(xIimage, 0, 0);
            }
            doc.Save(Path.Combine(destinationDir, destinationFile));
            doc.Close();
        }

        static string GetSourcePath(int i)
        {
            string fileName = string.Format(sourceFileTemplate, i);
            string filePath = Path.Combine(sourceDir, fileName);
            return filePath;
        }
    }
}
